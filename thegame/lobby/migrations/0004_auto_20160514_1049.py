# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-14 10:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lobby', '0003_auto_20160514_0911'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='state',
            field=models.CharField(choices=[('NS', 'Not Started'), ('RUN', 'Playing'), ('FIN', 'Finished')], default='NS', max_length=3),
        ),
    ]
