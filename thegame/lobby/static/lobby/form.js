function reg_tabs() {

   $('#login-form-link').click(function(e) {
      $("#login-form").delay(100).fadeIn(50);
      $("#register-form").fadeOut(50);
      $('#register-form-link').removeClass('active');
     $(this).addClass('active');
     e.preventDefault();
   });
   $('#register-form-link').click(function(e) {
     $("#register-form").delay(100).fadeIn(50);
     $("#login-form").fadeOut(50);
     $('#login-form-link').removeClass('active');
     $(this).addClass('active');
     e.preventDefault();
   });
 }

 window.addEventListener('load', function() {
   reg_tabs();
   if ($('#invalid').val()) {
     $('#register-form-link').click();
   }
 });
