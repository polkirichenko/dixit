String.prototype.format = function(placeholders) {
    var s = this;
    for(var propertyName in placeholders) {
        var re = new RegExp('{' + propertyName + '}', 'gm');
        s = s.replace(re, placeholders[propertyName]);
    }
    return s;
};


function make_button(type) {
    return '<input type="submit" value="{text}" class="btn btn-list" {disabled}/>'.format({
        text: type == 'leave' ? 'Leave' : 'Join',
        disabled: type == 'join-disabled' ? 'disabled' : ''
    });
}

function players_list(players) {
    var res = '';
    for (var i = 0; i < players.length; ++i) {
        res += players[i];
        if (i != players.length - 1)
            res += ', ';
    }
    return res;
}

function join(gid) {
    $.post(
        '/join/',
        {game_id: gid}
    ).always(
        function() {
            $('#new_game_btn').attr('disabled', 'disabled');
            update();
    });
}

function leave(gid) {
    $.post(
        '/leave/',
        {game_id: gid}
    ).always(
        function() {
            $('#new_game_btn').removeAttr('disabled');
            update();
    });
}

function button_submit(game_id, type) {
    return function(event) {
        event.preventDefault();
        if (type == 'leave')
            leave(game_id);
        else
            join(game_id);
    }
}

var glob_data = null;

function update() {

    $.get(
        '/api/games_info/'
    ).done(function(data) {
        if (glob_data == data) {
            return;
        }
        glob_data = data;
        var user = $('#user').val();
        var my_game = null;

        for (var game_id in data) {
            if (data[game_id][1].indexOf(user) != -1) {
                if (data[game_id][1].length == 3) {
                    window.location.replace('/game/' + game_id + '/');
                }
                my_game = game_id;
            }
        }

        $('#games_table tbody').empty();

        for (var game_id in data) {

            var type;
            if (my_game) {
                type = game_id == my_game ? 'leave' : 'join-disabled';
            } else {
                type = 'join';
            }

            
            var row =
                '<tr><td>' + data[game_id][0] +'</td>' +
                '<td>' + players_list(data[game_id][1]) + '</td>' +
                '<td class="join-clmn">' +
                '<form method="post" id="form_{game_id}">'.format({game_id: game_id}) +
                make_button(type) +
                '</form>' +
                '</td></tr>';

            $('#games_table tbody').append(row);
            
            $('#form_{game_id}'.format({game_id: game_id})).submit(
                button_submit(game_id, type)
            )
        }
    });
}

window.addEventListener('load', function() {

    $('#new_game').submit(
        function(event) {
            event.preventDefault();
            $.post(
            '/new_game/'
            ).always(
                function() {
                    $('#new_game_btn').attr('disabled', 'disabled');
                    update();
            });
        }
    )

    update();
    setInterval(update, 1000);
});