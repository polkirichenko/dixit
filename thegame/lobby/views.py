from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404

from .models import Game, Player, LoginForm, RegForm
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404


@login_required
def new_game(request):
    player = Player.objects.get(user=request.user)
    game = Game.objects.create(creator=player)
    game.players.add(player)
    game.save()
    return HttpResponse()


@login_required
def join(request):
    player = get_object_or_404(Player, user=User.objects.get(username=request.user))
    game = get_object_or_404(Game, id=int(request.POST['game_id']))
    game.players.add(player)
    game.save()
    return HttpResponse()


@login_required
def leave(request):
    player = get_object_or_404(Player, user=User.objects.get(username=request.user))
    game = get_object_or_404(Game, id=int(request.POST['game_id']))
    game.players.remove(player)
    game.save()
    if len(game.players.all()) == 0:
        game.delete()
    return HttpResponse()


@login_required
def game(request, game_id):
    player = Player.objects.get(user=request.user)
    game = get_object_or_404(Game, id=int(game_id))
    return render(request, 'game.html', context={'player_id': request.user.id, 'game_id': game_id})


@login_required
def game_list(request):
    player = Player.objects.get(user=request.user)
    return render(request, 'game_list.html', context={'player': player})


@login_required
def games_info(request):
    games = Game.objects.all().filter(state=Game.NOT_STARTED)
    return JsonResponse(dict([(g.pk, [g.creator.user.username, [pl.user.username for pl in g.players.all()]]) for g in games]))


def register(request):
    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        form = RegForm(request.POST)
        if form.is_valid():
            user = form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            login(request, user)
            return redirect(game_list)
        else:
            login_form = LoginForm()
            return render(request, 'home.html', context={'login_form': login_form, 'reg_form': form,
                                                         'invalid_log': '', 'invalid_reg': 'invalid'})


def my_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect(game_list)
    else:
        login_form = LoginForm()
        reg_form = RegForm()
        return render(request, 'home.html', context={'login_form': login_form, 'reg_form': reg_form,
                                                     'invalid_log': 'invalid', 'invalid_reg': ''})


def my_logout(request):
    logout(request)
    return redirect(home)


def home(request):
    login_form = LoginForm()
    reg_form = RegForm()
    return render(request, 'home.html', {'login_form': login_form, 'reg_form': reg_form,
                                         'invalid_log': '', 'invalid_reg': ''})
