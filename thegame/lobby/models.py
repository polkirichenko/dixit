from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class Player(models.Model):
    user = models.OneToOneField(User)
    # score, games
    # game = models.ForeignKey(Game, blank=True)

    def __str__(self):
        return self.user.get_username()


def create_user_profile(sender, instance, created, **kwargs):
    if created:
         profile, created = Player.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)


class Game(models.Model):
    NOT_STARTED = 'NS'
    RUNNING = 'RUN'
    FINISHED = 'FIN'

    GAME_STATE = (
        (NOT_STARTED, 'Not Started'),
        (RUNNING, 'Playing'),
        (FINISHED, 'Finished'),
    )

    state = models.CharField(max_length=3, choices=GAME_STATE, default='NS')
    creator = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='creator')
    players = models.ManyToManyField(Player, related_name='players')

    def __str__(self):
        return str(self.id) + ' ' + str(self.creator)


class RegForm(forms.ModelForm):
    confirm_password = forms.CharField(label="confirm_password", max_length=100,
                                       widget=forms.PasswordInput(attrs={
                                            'required': '',
                                            'autocomplete': 'off',
                                            'class': "form-control",
                                            'placeholder': 'Confirm Password',
                                       }))

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'confirm_password']
        widgets = {
            'username': forms.TextInput(
                attrs={
                    'required': '',
                    'autocomplete': 'off',
                    'class': "form-control",
                    'placeholder': 'Username',
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'required': '',
                    'autocomplete': 'off',
                    'class': "form-control",
                    'placeholder': 'Email Address',
                }
            ),
            'password': forms.PasswordInput(
                attrs={
                    'required': '',
                    'autocomplete': 'off',
                    'class': "form-control",
                    'placeholder': 'Password',
                }
            ),
        }
        labels = {
            'username': 'LOGIN',
            'password': 'PASSWORD',
            'email': 'EMAIL',
            'confirm_password': 'CONFIRM PASSWORD'
        }

    def clean_confirm_password(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('confirm_password')

        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("Your passwords do not match")
        return password2


class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password']
        widgets = {
            'username': forms.TextInput(
                attrs={
                    'required': '',
                    'autocomplete': 'off',
                    'class': "form-control",
                    'placeholder': 'Username',
                }
            ),
            'password': forms.PasswordInput(
                attrs={
                    'required': '',
                    'autocomplete': 'off',
                    'class': "form-control",
                    'placeholder': 'Password',
                }
            )

        }
        labels = {
            'username': 'LOGIN',
            'password': 'PASSWORD',
        }
