"""thegame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from lobby import views as lobby_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', lobby_views.home, name='home'),
    url(r'^login/$', lobby_views.my_login, name='login'),
    url(r'^logout/$', lobby_views.my_logout, name='logout'),
    url(r'^register/$', lobby_views.register, name='register'),
    url(r'^game_list/$', lobby_views.game_list, name='game_list'),
    url(r'^api/games_info/$', lobby_views.games_info, name='game_info'),
    url(r'^join/$', lobby_views.join, name='join'),
    url(r'^leave/$', lobby_views.leave, name='leave'),
    url(r'^new_game/$', lobby_views.new_game, name='new_game'),
    url(r'^game/(?P<game_id>[0-9]+)/$', lobby_views.game, name='game'),
]
