import asyncio
import random
import logging
import json
import contextlib
from aiohttp import web, websocket


class Player:
    def __init__(self, user_id, ws):
        self.id = user_id
        self.ws = ws
        self.game = None
        self.cards = []
        self.points = 0
        self.done = None # 0 or 1 meaning whether non-storyteller player made his move on "choose" and "vote" stages

    def replaceCard(self, card_id):
        pass
   

class Game:
    card_per_player = 6
    total_cards = 36
    states = ('story', 'choose', 'vote', 'points')
    def __init__(self, id, player):
        self.id = id
        self.players = {player.id : player}
        player.game = self.id

        self.free_cards = set(range(1, self.total_cards + 1))
        self.dealCards(player)

        self.storyteller = player.id
        self.vote = {}  # vote[card] - list of players who voted for card
        self.true_choice = {}  # choice[player] - card which player chose

        self.state = None

    def addPlayer(self, player):
        self.players[player.id] = player
        player.game = self.id
        self.dealCards(player)

    def dealCards(self, player):
        player.cards = random.sample(self.free_cards, self.card_per_player)
        self.free_cards.difference_update(player.cards)


def countPoints(vote, choice, stid):
    points = {pid : 0 for pid in choice.keys()}
    guess = len(vote[choice[stid]])
    if guess == 0 or guess == len(choice) - 1:
        points[stid] = -3
        return points
    else:
        points[stid] = 3
        for pl in vote[choice[stid]]:
            points[pl] = 3
        for pl in choice.keys():
            points[pl] += vote[choice[pl]]



cur_games = {}
last_story = {}

loop = asyncio.get_event_loop()
log = logging.getLogger("game")

async def game_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    game_id = request.match_info["game_id"]
    usr_id = request.match_info["usr_id"]

    if game_id not in cur_games:
        cur_games[game_id] = Game(game_id, Player(usr_id, ws))
        cur_games[game_id].state = 'story'
        last_story[game_id] = ""
    elif usr_id not in cur_games[game_id].players:
        cur_games[game_id].addPlayer(Player(usr_id, ws))
    cur_game = cur_games[game_id]

    async for msg in ws:
        if msg.tp == websocket.MSG_TEXT:
            log.info("RECEIVED MSG %s", str(msg))
            cont = json.loads(str(msg.data))

            if cont["type"] == "connection":
                p = cur_game.players[usr_id]
                tosend = {"type": "connect", "state": cur_game.state, "done": p.done,
                          "id" : p.id, "cards" : p.cards,
                          "st": (usr_id == cur_game.storyteller),
                          "story_text": last_story[game_id]}
                log.info("SENDING %s", str(tosend))
                ws.send_str(json.dumps(tosend))

            
            elif cont["type"] == "move":
                cur_game.true_choice[usr_id] = cont["card"]
                cur_game.vote[cont["card"]] = []
                
                if usr_id == cur_game.storyteller:
                    last_story[game_id] = cont["story"]
                    cur_game.state = "choose"
                    for i, player in cur_game.players.items():
                        if i == usr_id:
                            player.cards.remove(cont["card"])
                            tosend = {"type" : "story", "cards": player.cards, "st": True}
                            player.ws.send_str(json.dumps(tosend))
                            continue
                        player.done = False
                        tosend = {"type" : "story", "story_text" : cont["story"],
                                 "st": (i == cur_game.storyteller)}
                        player.ws.send_str(json.dumps(tosend))
                
                elif len(cur_game.vote) == len(cur_game.players):
                        cur_game.state = 'vote'
                        for_voting = list(cur_game.true_choice.values())
                        random.shuffle(for_voting)
                        await asyncio.sleep(0.5)
                        for i, player in cur_game.players.items():
                            tosend = {"type" : "vote", "cards" : for_voting}
                            player.ws.send_str(json.dumps(tosend))

                else:
                    # We made our move and now have to wait for the other players
                    log.info("GOOOOT HEREEEE")
                    player = cur_game.players[usr_id]
                    player.done = True
                    player.cards.remove(cont["card"])
                    tosend = {"type" : "choose_wait_others", "cards": player.cards, "st": False}
                    log.info("GONNNA SEND %s", str(tosend))
                    player.ws.send_str(json.dumps(tosend))


            elif cont["type"] == "vote":
                cur_game.vote[cont["card"]].append(usr_id)
                if sum(len(cur_game.vote[c]) for c in cur_game.vote.keys()) == len(cur_game.players) - 1:
                    tosend = {"type" : "res", "votes" : cur_game.vote, "st_card" : cur_game.true_choice[cur_game.storyteller]}
                    points = countPoints(cur_game.vote, cur_game.true_choice, cur_game.storyteller)
                    for i, player in cur_game.players.items():
                        tosend["points"] = points
                        player.ws.send_str(json.dumps(tosend))

        elif msg.tp == websocket.MSG_CLOSE:
            log.info("Error: %s", str(msg))
            break
        else:
            log.info("Strange msg: %s", str(msg))

    log.info("Closing websocket connection")
    return ws


async def start_server():
    app = web.Application()
    app.router.add_route("GET", "/game/{game_id}/{usr_id}", game_handler)
    server = await loop.create_server(app.make_handler(), '0.0.0.0', 8081)
    return server


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, format="%(asctime)s [%(levelname)s]: %(message)s")
    log.info("Starting server")

    loop.run_until_complete(start_server())
    loop.run_forever()
